/**
 * Defines two classes which abstracts away some of the functionality from the
 * BNO055 and the BMP280. Mostly converts values from metric to freedom units.
 */

#include <Adafruit_BMP280.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_Sensor.h>
#include <BasicLinearAlgebra.h>
#include <utility/imumaths.h>

#ifndef SENSORS_H
#define SENSORS_H

#define METERTOFEET 3.28084
#define G 32.17405

#define LINEAR Adafruit_BNO055::VECTOR_LINEARACCEL
#define RAW Adafruit_BNO055::VECTOR_ACCELEROMETER
#define GRAVITY Adafruit_BNO055::VECTOR_GRAVITY

using Vec3 = BLA::Matrix<3, 1>;
using Mat3 = BLA::Matrix<3, 3>;

/**
 *  Connections
 *  ===============================
 *  Connect SCL to analog 5
 *  Connect SDA to analog 4
 *  Connect VDD to 3.3-5V DC
 *  Connect GROUND to common ground
 */

// Convert imumath vecs to BLA matrices, which are much easier to use
Vec3 imu_to_bla(imu::Vector<3> &vec) {
    Vec3 bla_vec = {vec.x(), vec.y(), vec.z()};
    return bla_vec;
}

class BMP280 {
   private:
    Adafruit_BMP280 sensor;

   public:
    void attach_sensor(Adafruit_BMP280 bmp) {
        sensor = bmp;
        // BMP's "settings"
        sensor.setSampling(
            Adafruit_BMP280::MODE_NORMAL,  /* Operating Mode. */
            Adafruit_BMP280::SAMPLING_X16, /* Temp. oversampling */
            Adafruit_BMP280::SAMPLING_X16, /* Pressure oversampling */
            Adafruit_BMP280::FILTER_X16,   /* Filtering. */
            Adafruit_BMP280::STANDBY_MS_1);
    }

    /**
     * Because we're only concerned with altitude above the ground, we can
     * ignore the pressure and set the target altitude to equal the target plus
     * the altitude we read when the system is first turned on
     */
    double altitude() { return sensor.readAltitude(1013.25) * METERTOFEET; }
};

class BNO055 {
   private:
    Adafruit_BNO055 sensor;

   public:
    void attach_sensor(Adafruit_BNO055 bno) { sensor = bno; }

    Vec3 linear_acceleration() {
        // prints linear acceleration from accelerometer
        imu::Vector<3> accel = sensor.getVector(LINEAR);
        Vec3 bla_accel = imu_to_bla(accel);
        return bla_accel * METERTOFEET;
    }

    Vec3 acceleration() {
        // prints linear acceleration from accelerometer
        imu::Vector<3> accel = sensor.getVector(RAW);
        Vec3 bla_accel = imu_to_bla(accel);
        return bla_accel * METERTOFEET;
    }

    Vec3 gravity() {  // prints linear acceleration from accelerometer
        imu::Vector<3> accel = sensor.getVector(GRAVITY);
        Vec3 bla_accel = imu_to_bla(accel);
        return bla_accel * METERTOFEET;
    }

    /**
     * Return the BNO's quaternion values
     */
    imu::Quaternion quaternions() { return sensor.getQuat(); }

    /**
     * Creates a transformation matrix to transform acceleration in the body
     * reference frame into the inertial frame, i.e the world frame.
     */
    Mat3 transformation_matrix() {
        imu::Quaternion quat = quaternions();
        double w = quat.w();
        double x = quat.x();
        double y = quat.y();
        double z = quat.z();

        Mat3 t_mat = {w * w + x * x - y * y - z * z, 2 * x * y - 2 * w * z,
                      2 * x * z + 2 * w * y,         2 * x * y + 2 * w * z,
                      w * w - x * x + y * y - z * z, 2 * y * z - 2 * w * x,
                      2 * x * z - 2 * w * y,         2 * y * z + 2 * w * x,
                      w * w - x * x - y * y + z * z};
        return t_mat;
    }

    /**
     * Convert the linear acceleration from the BNO into one expressed
     * in the world reference frame
     */
    Vec3 inertial_acceleration() {
        Mat3 trans_mat = transformation_matrix();
        Vec3 accel = linear_acceleration();
        return trans_mat * accel;
        ;
    }

    /**
     * Calculate the inertial linear acceleration and return the vertical
     * component
     */
    double vertical_acceleration() {
        Vec3 accel = inertial_acceleration();
        return accel(2);
    }
};

#endif  // SENSORS_H
