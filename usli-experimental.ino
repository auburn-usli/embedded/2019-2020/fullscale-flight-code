#include <Wire.h>
#include "logger.h"
#include "sensors.h"
#include "vehicle.h"

#define LAUNCH_TRIGGER 50
#define DRAG_TRIGGER 1000
#define STEP 50

enum Runmode {
    STANDBY = (1 << 0),  // Sitting on the launch pad
    LAUNCH = (1 << 1),   // motor is burning, waiting for burn out
    DRAG = (1 << 2),     // drag routinne (for subscale just log data)
    DESCENT = (1 << 3),  // stop drag and data logging and close stuff down
    GO = (1 << 4),       // flight is all good to go
    NOGO = (1 << 5),     // flight is a bust
    NO_ALT = (1 << 6),   // BMP280 failed to connect
    NO_IMU = (1 << 7),   // BNO055 failed to connect
    NO_SD = (1 << 8)     // SD card failed to connect
};

String headers[] = {"Time [s]", "Acceleration [ft/s^2]", "Velocity [ft/s]",
                    "Altitude [ft]", "Proj. Altitude [ft]"};

DataLogger datalog;
EventLogger eventlog;

BitFlag MODE;  // abstract away bitwise operations on flag

Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28);
Adafruit_BMP280 bmp;

BMP280 altimeter;
BNO055 imu_sensor;

double apogee = 0;
double ground_alt = 0;
Vec3 state = {0, 0, 0};

void setup() {
    // Gp ahead and enter standby mode
    MODE.enable(STANDBY | GO);
    Serial.begin(115200);
    pinMode(LED_PIN, OUTPUT);

    if (!SD.begin(chipSelect)) {
        Serial.println("Initialization failed!");
        MODE.disable(GO);
        MODE.enable(NO_SD);  // turn off GO and turn on NO_IMU
    }

    if (MODE == NO_SD) {
        while (1) {  // hang and blink, so we know the SD card isn't in
            delay(50);
            blink_led();
        }
    }

    // Since we're here SD card is good so create data and events files
    datalog.initialize("DATA", headers, 5);
    eventlog.initialize("LOG");

    // try connection to imu
    if (!bno.begin()) {
        Serial.println("BNO did not connect");
        MODE.enable(NO_IMU);  // turn off GO and tu rn on NO_IMU
        eventlog.error("BNO055 failed to connect");
    } else {
        eventlog.event("BNO055 connected successfully");
        imu_sensor.attach_sensor(bno);
    }

    // try connection to altimeter
    if (!bmp.begin()) {
        Serial.println("Problem with altimeter.");
        MODE.enable(NO_ALT);
        eventlog.error("BMP280 failed to connect");
    } else {
        eventlog.event("BMP280 connected successfully");
        altimeter.attach_sensor(bmp);
        ground_alt = altimeter.altitude();
        eventlog.event("Ground altitude: " + String(ground_alt) + " ft");
    }

    if (MODE == (NO_IMU | NO_ALT)) {
        MODE.enable(NOGO);
        MODE.disable(GO);
        eventlog.error("BNO055 and BMP280 failed to connect - flight is NOGO");
    }

    if (MODE == GO) {
        // led on constantly means we're good
        digitalWrite(LED_PIN, HIGH);
    }

    // this block runs if flight is a no go
    else if (MODE == NOGO) {
        while (1) {
            delay(1000);
            blink_led();
        }
    }

    state(0) = altimeter.altitude();
    state(2) = imu_sensor.vertical_acceleration();
}

void loop() {
    delay(STEP);  // give the sensors some time to update their values

    double dt = elapsed_time();
    double alt = altimeter.altitude();
    double accel = imu_sensor.vertical_acceleration();

    /**
     * All sensors are connected and everything is ok, so run normally
     */
    if (MODE != NO_ALT && MODE != NO_IMU) {
        state_update(state, alt, accel, dt);
        double p_alt = projected_altitude(state);
        Serial.println(String(state(1)) + ',' + state(2));
    }

    // Altimeter is connected, but not the IMU
    else if (MODE == NO_IMU || MODE != NO_ALT) {
        Serial.println("IMU is not connected");
    }

    // IMU is connected, but not the altimeter
    else if (MODE != NO_IMU || MODE == NO_ALT) {
        Serial.println("BMP is not connected");
    }
}
