/**
 * For data and event logging. Depending on how a Logger object is created,
 * it will write data to a text file in different ways. There are methods
 * for writing data in columns like an Excel table, and others for writing
 * events and error messages. Files are created using the initialize function
 * rather than on class instantiation, in case SD card is not connected
 * properly.
 */

#ifndef _DATALOG_h
#define _DATALOG_h
#include <SD.h>
#include <SPI.h>
#include <math.h>
#include <stdio.h>

#define NUMCOL 32
#define COLWIDTH 20

const int chipSelect = BUILTIN_SDCARD;

String repeating_char(const char c, int repeats) {
    String str;
    for (int i = 0; i < repeats; i++) {
        str += c;
    }
    return str;
}

/**
 * Base Logger class, handles creating files and prevents overwriting old
 * files.
 */
class Logger {
   protected:
    String filename;

   public:
    // Create a indent to pad values in a column
    String create_indent(int num_spaces) {
        String pad_str = String("");
        for (int i = 0; i < num_spaces; ++i) {
            pad_str += String(" ");
        }
        return pad_str;
    }

    // Number of whole digits for each data point
    int data_len(double data) { return String(data).length(); }

    // Create a unique file name every time the microcontroller boots, so we
    // don't overwrite any data on accident. Prefix stands for the beginning of
    // the file name, like DATA or LOGS
    String create_file_name(const char *prefix) {
        int num_files = 0;
        File root = SD.open("/");  // start at root and walk all files
        while (true) {
            File entry = root.openNextFile();
            if (!entry)  // no more files
                break;
            else if (entry.isDirectory())  // skip over directories
                continue;
            else {
                String current_file = entry.name();
                if (current_file.startsWith(prefix)) num_files++;
            }
        }
        String new_filename = String(prefix) + "_" + num_files + ".txt";
        return new_filename;
    }
};

/**
 * DataLogger class derived from Logger class, handles writing data in a fixed
 * width column format
 */

class DataLogger : public Logger {
    String headers[NUMCOL];
    double data_array[NUMCOL];
    int num_hdr;

   public:
    void initialize(const char *name, String header_array[], int num_headers) {
        num_hdr = num_headers;
        filename = create_file_name(name);
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file) {
            String line;
            for (int i = 0; i < num_hdr; i++) {
                headers[i] = header_array[i];
                String pad_str = create_indent(COLWIDTH - headers[i].length());
                if (i == 0)
                    line += String(headers[i]) + pad_str;
                else
                    line += pad_str + String(headers[i]);
            }
            file.println(line);
        }
        file.close();
    }
    // pretty print text to file
    void data_write() {
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file) {
            String line;
            for (int i = 0; i < num_hdr; i++) {
                String data = String(data_array[i]);
                int indent = COLWIDTH - data.length();
                String pad_str = create_indent(indent);
                if (i == 0)
                    line += data + pad_str;
                else
                    line += pad_str + data;
            }
            file.println(line);
        }
        file.close();
    }
    // Add data to array that Logger writes to file
    double &operator[](int index) { return data_array[index]; }
};

/**
 * EventLogger class derived from Logger, for logging actions and events during
 * operation.
 */

class EventLogger : public Logger {
   private:
    float current_time;
    float prev_time;
    float seconds;
    float minutes;
    float hours;

   public:
    void initialize(const char *name) {
        filename = create_file_name(name);
        current_time = 0;
        prev_time = 0;
        seconds = 0;
        minutes = 0;
        hours = 0;
    }

    // For event logs, creates a formatted time stamp string
    String time_stamp() {
        current_time = millis() / 1000;
        seconds = current_time - prev_time;
        if (seconds >= 60) {
            prev_time = current_time;
            minutes += 1;
        }
        if (minutes >= 60) {
            hours += 1;
            minutes = 0;
        }
        int hour = (int)hours;
        int minute = (int)minutes;
        int second = (int)seconds;
        String hour_str = hour < 10 ? "0" + String(hour) : String(hour);
        String min_str = minute < 10 ? "0" + String(minute) : String(minute);
        String sec_str = second < 10 ? "0" + String(second) : String(second);
        return hour_str + ":" + min_str + ":" + sec_str;
    }
    // Simple writeln
    void writeln(String line) {
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file) file.println(line);
        file.close();
    }

    void error(String msg) {
        String time = time_stamp();
        String line = "[ " + time + " ][ ERROR ]    " + msg;
        writeln(line);
    }

    void event(String msg) {
        String time = time_stamp();
        String line = "[ " + time + " ][ EVENT ]    " + msg;
        writeln(line);
    }
};

#endif
