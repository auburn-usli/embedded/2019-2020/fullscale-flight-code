/**
 * Contains "utility" functions that does extra things like blink the builtin
 * led, calculate the time between loops, etc. BitFlag is for creating an
 * integer whose bits can be turned on and off, so it is basically a
 * multi-boolean variable. Mutliple "flags" can be combined to make more complex
 * decisions.
 */

#ifndef VEHICLE_H
#define VEHICLE_H

#include <BasicLinearAlgebra.h>
#include <math.h>

#define LED_PIN 13

#ifndef GRAV
#define GRAV 32.17405
#endif

// Standard deviations for bmp and bno, needed for complimentary filter
#define BMP_STD 1
#define BNO_STD 0.5
#define STD_RAT (BNO_STD / BMP_STD)

using Vec2 = BLA::Matrix<2, 1>;
using Vec3 = BLA::Matrix<3, 1>;
using Mat3 = BLA::Matrix<3, 3>;
using Mat2 = BLA::Matrix<2, 2>;

/**
 * Return the amount of time that has passed since this function was last
 * called.
 */
double elapsed_time() {
    static double prev_time = 0;
    double now = millis();
    double elapsed = now - prev_time;
    prev_time = now;
    return elapsed / 1000;
}

/**
 * "Toggle" the LED each time it is called.
 */
void blink_led() {
    static bool pin_on = false;
    pin_on ? digitalWrite(LED_PIN, LOW) : digitalWrite(LED_PIN, HIGH);
    pin_on = !pin_on;
}

/**
 * This function is the bread and butter of the system. It calculates the state
 * using the accelerometer and altimiter, and combines them to estimate the
 * vertical speed accurately. A state vector is passed by reference so it will
 * be modified instead of returning a new vector.
 */
void state_update(Vec3 &state, double alt, double accel, double dt) {
    Mat2 A = {1, dt, 0, 1};
    Mat2 B = {1, dt / 2, 0, 1};
    Vec2 Kc = {pow(2 * STD_RAT, 0.5), STD_RAT};  // filter
    Vec2 C = {dt / 2, 1};
    state(0, 1) = A * state + (B * Kc * (alt - state(0)) + C * accel) * dt;
    state(2) = accel;
}

/**
 * An abstraction over bitwise operations on a BitFlag, so this is basically a
 * flag You can combine flags to turn a bunch of things on and off at once. To
 * combine them, write flag1 | flag2, so to enable two flags, write
 * bitflag.enable(FLAG1 | FLAG2); etc...
 */
class BitFlag {
    int m_flag;

   public:
    BitFlag() { m_flag = 0; }
    void enable(const int &flags) { m_flag |= flags; }
    void disable(const int &flags) { m_flag &= ~flags; }
    void toggle(const int &flags) { m_flag ^= flags; }
    bool operator==(const int &flag) { return (m_flag & flag) == flag; }
    bool operator!=(const int &flag) { return !((m_flag & flag) == flag); }
};

// For the pid controller later
double projected_altitude(Vec3 &state) {
    double alt = state(0);
    double veloc = state(1);
    double accel = state(2);
    return abs((veloc * veloc) / (2 * (accel + GRAV))) *
               log(abs(accel / GRAV)) +
           alt;
}

#endif  // VEHICLE_H
